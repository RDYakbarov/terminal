<?php
/**
 * Created by PhpStorm.
 * User: rdyakbarov
 * Date: 04.04.17
 * Time: 23:16
 */
$temp_fifo_file = '/tmp/dolphin-pipe-'.uniqid('dolph');
if (!posix_mkfifo($temp_fifo_file, 0600)) {
    echo "Fatal error: Cannot create fifo file: something wrong with the system.\n";
    exit(1);
}

function deleteTempFifo() { unlink($GLOBALS['temp_fifo']); }
register_shutdown_function('deleteTempFifo');

$cmdfp = fopen($temp_fifo_file, 'r+');
stream_set_blocking($cmdfp, 0);